const Joi = require('@hapi/joi')

const registerValidation = data => {
    const schema = Joi.object({
        username: Joi.string().min(6).required(),
        password: Joi.string().min(6).required(),
    })

    return schema.validate(data, {
        allowUnknown: true
    })
}

const loginValidation = data => {
    const schema = Joi.object({
        username: Joi.string().min(6).required(),
        password: Joi.string().min(6).required()
    })
    
    return schema.validate(data, {
        allowUnknown: true
    })
}

module.exports.registerValidation = registerValidation
module.exports.loginValidation = loginValidation