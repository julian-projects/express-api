const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

require('dotenv/config');

const app = express();

// Routes
const authRoutes = require('./routes/auth');
const classroomRoutes = require('./routes/classrooms');
const teacherRoutes = require('./routes/teachers');
const studentRoutes = require('./routes/students');
const roleRoutes = require('./routes/roles');
const subjectRoutes = require('./routes/subjects');

const { auth } = require('./middleware/auth')

// Middleware
app.use(cors());
app.use(bodyParser.json());
app.use('/api/v1/user', authRoutes);
app.use('/api/v1/classrooms', classroomRoutes);
app.use('/api/v1/teachers', teacherRoutes);
app.use('/api/v1/students', auth, studentRoutes);
app.use('/api/v1/roles', roleRoutes);
app.use('/api/v1/subjects', subjectRoutes);

mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true }, (err) => {
    if (err) return console.log("Error: ", err);
    console.log("connected to db");
})


app.listen(8080);