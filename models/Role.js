const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RoleSchema = Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    level: {
        type: Number,
        required: true
    },
    description: {
        type: String,
    }
});

module.exports = mongoose.model('Role', RoleSchema);