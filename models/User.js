const mongoose = require('mongoose');
const Enums = require('../enums');
const Schema = mongoose.Schema;

const UserSchema = Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        enum: Enums.GENDER,
        required: true
    },
    role: {
        type: Schema.Types.ObjectId,
        ref: 'Role',
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    lastChanged: {
        type: Date,
        default: Date.now
    },
    contact: {
        email: {
            type: String
        },
        mobile: {
            type: String
        },
        phone: {
            type: String
        }
    },
});

module.exports = mongoose.model('User', UserSchema);