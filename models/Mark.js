const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MarkSchema = Schema({
    mark: {
        type: String,
        required: true,
    },
    description: {
        type: String,
    },
    subject: {
        type: Schema.Types.ObjectId,
        ref: 'Subject',
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    lastChanged: {
        type: Date,
        default: Date.now
    },
});

module.exports = mongoose.model('Mark', MarkSchema);