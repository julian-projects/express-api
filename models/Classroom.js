const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ClassroomSchema = Schema({
    year: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    lastChanged: {
        type: Date,
        default: Date.now
    },
    primaryTeacher: {
        type: Schema.Types.ObjectId,
        ref: 'Teacher'
    },
    teachers: [{ type: Schema.Types.ObjectId, ref: 'Teacher' }],
    students: [{ type: Schema.Types.ObjectId, ref: 'Student' }]
});

module.exports = mongoose.model('Classroom', ClassroomSchema);