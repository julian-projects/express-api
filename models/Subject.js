const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SubjectSchema = Schema({
    name: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    lastChanged: {
        type: Date,
        default: Date.now
    },
    classroom: {
        type: Schema.Types.ObjectId,
        ref: 'Classroom',
        required: true
    },
    teacher: {
        type: Schema.Types.ObjectId,
        ref: 'Teacher',
        required: true
    },
    students: [{ type: Schema.Types.ObjectId, ref: 'Student' }]
});

module.exports = mongoose.model('Subject', SubjectSchema);