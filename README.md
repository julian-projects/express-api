## Using
express, body-parser, nodemon, mongoose, cors and dotenv  

## Curl examples
Get all  
curl -i -X GET http:/localhost:3000/posts  
Get one  
curl -X GET http://localhost:3000/posts/609a18721090d168fdbaf18d  
Post  
curl -i -X POST -H "Content-Type: application/json" -d '{"title": "Cool title2", "description": "Cool description"}' http://localhost:3000/posts  
Delete  
curl -X DELETE http://localhost:3000/posts/609a18721090d168fdbaf18d  
Patch  
curl -i -X PATCH -H "content-type: application/json" -d '{"title": "New cool title"}'  http:/localhost:3000/posts/609a1763736a5268de59e058  