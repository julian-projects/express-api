const Role = require('../models/Role');

const User = {
    GenerateEmail: async (firstname, lastname, roleId) => {
        if(!firstname) {
            throw "Firstname incorrect or missing"
        }

        if(!lastname) {
            throw "Lastname incorrect or missing"
        }

        if(!roleId) {
            throw "RoleId incorrect or missing"
        }

        try {
            const role = await Role.findById(roleId);
            return `${firstname.toLowerCase()}.${lastname.toLowerCase()}@${role.name.toLowerCase()}.schoolname.com`;
        } catch (err) {
            console.log(err);
        }
    }
}

module.exports = User;