const express = require('express');

const Model = require('../models/Teacher');
const UserService = require('../services/UserService');
const router = express.Router();

// GET ALL
router.get('/', async (req, res) => {
    try {
        const models = await Model.find()
            .sort({ firstname: 1, lastname: 1 })
            .skip(req.query.items * (req.query.page - 1))
            .limit(parseInt(req.query.items))
            .populate('role');
            
        res.json(models);
    } catch (err) {
        res.status(404).json({ message: err });
    }

});

// POST NEW
router.post('/', async (req, res) => {
    try {
        const model = new Model({
            ...req.body
        });

        try {
            model.contact.email = await UserService.GenerateEmail(model.firstname, model.lastname, model.role);
        } catch (err) {
            res.status(400).json({ message: err });
        }

        const savedModel = await model.save();
        res.status(201).json(savedModel);
    } catch (err) {
        console.log("error");
        res.status(400).json({ message: err });
    }
});

// GET ONE
router.get('/:id', async (req, res) => {
    try {
        const model = await Model.findById(req.params.id).populate('role');
        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

// DELETE
router.delete('/:id', async (req, res) => {
    try {
        const model = await Model.remove({ _id: req.params.id });
        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

// UPDATE
router.patch('/:id', async (req, res) => {
    try {
        const model = await Model.updateOne({ _id: req.params.id }, {
            $set: {
                ...req.body,
                lastChanged: new Date()
            }
        });
        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

module.exports = router;