const express = require('express');

const Model = require('../models/Role');
const router = express.Router();

// GET ALL
router.get('/', async (req, res) => {
    try {
        const models = await Model.find().sort({ level: 1 });

        res.json(models);
    } catch (err) {
        res.status(404).json({ message: err });
    }

});

// POST NEW
router.post('/', async (req, res) => {
    const model = new Model({
        ...req.body
    });

    try {
        const savedModel = await model.save();
        res.status(201).json(savedModel);
    } catch (err) {
        console.log("error");
        res.status(400).json({ message: err });
    }
});

// GET ONE
router.get('/:id', async (req, res) => {
    try {
        const model = await Model.findById(req.params.id);
        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

// DELETE
router.delete('/:id', async (req, res) => {
    try {
        const model = await Model.remove({ _id: req.params.id });
        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

// UPDATE
router.patch('/:id', async (req, res) => {
    try {
        const model = await Model.updateOne({ _id: req.params.id }, {
            $set: {
                ...req.body
            }
        });
        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

module.exports = router;