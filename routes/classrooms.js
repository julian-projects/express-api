const express = require('express');
const mongoose = require('mongoose');

const Model = require('../models/Classroom');
const Student = require('../models/Student');
const Teacher = require('../models/Teacher');
const router = express.Router();

// GET ALL
router.get('/', async (req, res) => {
    try {
        const models = await Model.find()
            .sort({ year: 1, name: 1 })
            .skip(req.query.items * (req.query.page - 1))
            .limit(parseInt(req.query.items))
            .populate('primaryTeacher')
            .populate('teachers')
            .populate('students');

        res.json(models);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

// POST NEW
router.post('/', async (req, res) => {
    const model = new Model({
        ...req.body
    });

    try {
        const savedModel = await model.save();
        res.status(201).json(savedModel);
    } catch (err) {
        console.log("error");
        res.status(400).json({ message: err });
    }
});

// GET ONE
router.get('/:id', async (req, res) => {
    try {
        const model = await Model.findById(req.params.id)
            .populate('primaryTeacher')
            .populate('teachers')
            .populate('students');

        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

// DELETE
router.delete('/:id', async (req, res) => {
    try {
        const model = await Model.remove({ _id: req.params.id });
        
        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

// UPDATE
router.patch('/:id', async (req, res) => {
    try {
        const model = await Model.updateOne({ _id: req.params.id }, {
            $set: {
                ...req.body,
                lastChanged: new Date()
            }
        });
        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

// ADD STUDENT
router.post('/:id/students/:studentId', async (req, res) => {
    try {
        const model = await Model.updateOne(
            { _id: req.params.id },
            { $addToSet: { students: req.params.studentId } }
        );
        const student = await Student.updateOne(
            { _id: req.params.studentId },
            {
                $set: {
                    classroom: req.params.id
                }
            }
        )
        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

// REMOVE STUDENT
router.delete('/:id/students/:studentId', async (req, res) => {
    try {
        const model = await Model.updateOne(
            { _id: req.params.id },
            { $pull: { students: mongoose.Types.ObjectId(req.params.studentId) } }
        );
        const student = await Student.updateOne(
            { _id: req.params.studentId },
            {
                $set: {
                    classroom: null
                }
            }
        )
        res.json(model);
    } catch (err) {
        console.log(err);
        res.status(404).json({ message: err });
    }
});

// ADD TEACHER
router.post('/:id/teachers/:teacherId', async (req, res) => {
    try {
        const model = await Model.updateOne(
            { _id: req.params.id },
            { $addToSet: { teachers: req.params.teacherId } }
        );
        const teacher = await Teacher.updateOne(
            { _id: req.params.teacherId },
            {
                $set: {
                    classroom: req.params.id
                }
            }
        )
        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

// REMOVE TEACHER
router.delete('/:id/teachers/:teacherId', async (req, res) => {
    try {
        const model = await Model.updateOne(
            { _id: req.params.id },
            { $pull: { teachers: mongoose.Types.ObjectId(req.params.teacherId) } }
        );
        const teacher = await Teacher.updateOne(
            { _id: req.params.teacherId },
            {
                $set: {
                    classroom: null
                }
            }
        )
        res.json(model);
    } catch (err) {
        console.log(err);
        res.status(404).json({ message: err });
    }
});

module.exports = router;