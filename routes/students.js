const express = require('express');
const mongoose = require('mongoose');

const Model = require('../models/Student');
const Mark = require('../models/Mark');
const UserService = require('../services/UserService');
const router = express.Router();

// GET ALL
router.get('/', async (req, res) => {
    try {
        const models = await Model.find()
            .sort({ firstname: 1, lastname: 1 })
            .skip(req.query.items * (req.query.page - 1))
            .limit(parseInt(req.query.items))
            .populate('role')
            .populate({
                path: 'marks',
                populate: {
                    path: 'subject',
                    model: 'Subject'
                }
            })
            .populate('classroom');

        res.json(models);
    } catch (err) {
        res.status(404).json({ message: err });
    }

});

// POST NEW
router.post('/', async (req, res) => {
    try {
        const model = new Model({
            ...req.body
        });

        try {
            model.contact.email = await UserService.GenerateEmail(model.firstname, model.lastname, model.role);
        } catch (err) {
            res.status(400).json({ message: err });
        }

        const savedModel = await model.save();
        res.status(201).json(savedModel);
    } catch (err) {
        console.log("error");
        res.status(400).json({ message: err });
    }
});

// GET ONE
router.get('/:id', async (req, res) => {
    try {
        const model = await Model.findById(req.params.id)
            .populate('role')
            .populate({
                path: 'marks',
                populate: {
                    path: 'subject',
                    model: 'Subject'
                }
            })
            .populate('classroom');

        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

// DELETE
router.delete('/:id', async (req, res) => {
    try {
        const model = await Model.remove({ _id: req.params.id });

        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

// UPDATE
router.patch('/:id', async (req, res) => {
    try {
        const model = await Model.updateOne(
            { _id: req.params.id },
            {
                $set: {
                    ...req.body,
                    lastChanged: new Date()
                }
            }
        );

        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

// POST NEW MARK
router.post('/:id/marks', async (req, res) => {
    try {
        const mark = new Mark({
            ...req.body,
            subject: mongoose.Types.ObjectId(req.body.subject),
        });

        try {
            const savedMark = await mark.save();

            const model = await Model.updateOne(
                { _id: req.params.id },
                { $push: { marks: savedMark } }
            );

            res.json(model);

        } catch (err) {
            console.log(err);
            res.status(400).json({ message: err });
        }
    } catch (err) {
        console.log(err);
        res.status(404).json({ message: err });
    }
});

// DELETE MARK
router.delete('/:id/marks/:markId', async (req, res) => {
    try {
        const model = await Model.updateOne(
            { _id: req.params.id },
            { $pull: { marks: mongoose.Types.ObjectId(req.params.markId) } }
        );

        res.json(model);
    } catch (err) {
        res.status(404).json({ message: err });
    }
});

module.exports = router;