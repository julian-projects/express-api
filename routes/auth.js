const express = require('express')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const router = express.Router()
const Model = require('../models/User')

const { registerValidation, loginValidation } = require('../validation/user')

router.post('/register', async (req, res) => {

    // Validation
    const { error } = registerValidation(req.body)

    if (error)
        res.status(400).send(error)

    // Check if user already exists in db
    const usernameExists = await Model.findOne({ username: req.body.username })
    if (usernameExists)
        res.status(400).send('Username already exists')

    // Hash the password
    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash(req.body.password, salt)

    // New user from schema
    const model = new Model({
        ...req.body,
        password: hashedPassword
    });

    try {
        const savedModel = await model.save();
        res.status(201).json({ user: savedModel._id });
    } catch (err) {
        console.log("error");
        res.status(400).json({ message: err });
    }

    res.send('Register')
})

router.post('/login', async (req, res) => {

    // Validation
    const { error } = loginValidation(req.body)

    if (error)
        res.status(400).send(error)

    // Check if user exists in db
    const user = await Model.findOne({ username: req.body.username })
    if (!user)
        res.status(400).send('Username or password is wrong')

    // Check password
    const validPassword = await bcrypt.compare(req.body.password, user.password)
    if (!validPassword)
        res.status(400).send('Username or password is wrong')

    // Create and assign json token
    const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET)
    res.header('auth-token', token).send(token)

})

module.exports = router